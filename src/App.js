// Importing modules
import React, { useState, useEffect } from "react";
import "./tabledata.css";
import logo from './arvida.svg';
import "./App.css";

                
function TableData() {

        const [data, getData] = useState([])
        const endpoint = "/items"
        useEffect(() =>{
                fetchData()
        },[])

        const fetchData = () => {
                fetch(endpoint)
                        .then(res =>
                                res.json())
                        .then( response => {
                                console.log(response);
                                getData(response);
        })}

        return(
        <>

        <img src={logo} className="App-logo" alt="logo" />
	    
            <h1>Demo App: Display API items</h1>
            <tbody>
                <tr>
                    
                    <th>Id</th>
                    <th>Title</th>
                    <th>Content</th>
                </tr>
                {data.map((item, i) => (
                    <tr key={i}>
                        <td>{item.id}</td>
                        <td>{item.title}</td>
                        <td>{item.content}</td>
                    </tr>
                ))}
            </tbody>

        </>
            );
}

export default TableData;
